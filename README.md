# bitwarden-deduplicate

This tool removes duplicate entries in a Bitwarden vault export.

Duplicate is determined by comparing items after removing their "id" fields, 
using Clojure's `=` function.

# Usage

Steps to use this tool:

1. Export your Bitwarden vault in the JSON format (we obviously can't process the encrypted version).
2. Ensure you have a working version of [babashka](https://github.com/babashka/babashka) installed.
3. Run the tool `./bitwarden-deduplicate.clj /path/to/bw-vault-export-file.json`
4. Inspect the resulting output file `deduplicated.json`
5. Once you feel good about things you need to delete all items in your vault (you will lose Folder info I believe)
6. Re-import the deduplicated file using the import tool.
