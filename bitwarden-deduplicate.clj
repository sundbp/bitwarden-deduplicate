#!/usr/bin/env bb

(require '[cheshire.core :as json])

(defn has-item?
  "we consider an item to already exist if the the items without ID are
   equivalent according to the rules of =
   to determine this we check if there is at least one match of i in items"
  [items i]
  (let [without-id (map (fn [x] (dissoc x "id")) items)
        i (dissoc i "id")
        dupe (->> without-id
                  (filter (fn [x] (= x i)))
                  first)]
    (not (nil? dupe))))

(when (not= 1 (count *command-line-args*))
  (println "usage: bitwarden-deduplicate.clj bitwarden-export-file.json")
  (System/exit 1))

(let [content (-> *command-line-args*
                  first
                  slurp
                  json/parse-string)
      in-items (-> content (get "items"))
      de-duped (reduce (fn [res x]
                         (if (has-item? res x)
                           res
                           (conj res x)))
                       []
                       in-items)]
  (println "Input had" (count in-items) "items. After removing duplicates we have" (count de-duped) "items.")
  (->> (json/generate-string (assoc content "items" de-duped) {:pretty true})
       (spit "deduplicated.json"))
  (println "De-duped output written to: deduplicated.json"))
